import axios from "axios";


export default class RpcClient {

    private username: string;
    private password: string;
    private host: string;
    private port: number;

    constructor(host:string, port: number, username: string, password: string) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    /**
     * Structures the method name and paramaters into an acceptable format for the JSON-RPC Server to parse.
     */
    private getRPCFormat = (method: string, params: any[]) => {
        return {
            method: method,
            params: params,
            id: 1
        }
    }

    /**
     * Returns the HTTP URL to the RPC Server.
     */
    private getURL = () => `http://${this.host}:${this.port}/`;

    /**
     * Will send an axios request to the RPC Server and return the response.
     */
    public sendRPCRequest = async (method: string, ...params: any[]) => {
        let Request = this.getRPCFormat(method, params);
        try {
            let result = await axios.post(this.getURL(), Request, {
                auth: {
                    username: this.username,
                    password: this.password
                }
            });
            return result.data;
        } catch (e) {
            if(e?.response?.data?.error?.message == 'Block height out of range') return 'Stable';
            console.log('[RPC] Unable to reach RPC Server - ERROR:', e.response ?  e.response : 'NoResponse' );
            return false;
        }
    }

}
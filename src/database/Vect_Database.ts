import {Pool, PoolClient} from 'pg';
import Vect from '../models/Vect';

export default class Vec_Database {
    private pool : Pool;
    
    constructor(pool : Pool) {
        this.pool = pool;
    }

    public createVect = (vect: Vect, client: PoolClient):Promise<string|false> => {
        return new Promise(async (resolve) => {
            try {
                let vectId = this.uuidv4();
                await client.query(`INSERT INTO public.vect(
                    id, transaction, n, value, type, spent, consumed_tx, asm, address)
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);`,
                [
                    vectId,
                    vect.getTransaction(),
                    vect.getN(),
                    vect.getValue(),
                    vect.getType(),
                    false,
                    null,
                    vect.getAsm(),
                    vect.getAddress()
                ]);
                return resolve(vectId);
            } catch (e) {
                console.log(e);
                return resolve(false);
            } 
        });
    }

    public getUnspentVout = async (txid:string, vout: number, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                let unspentVout = await client.query(`SELECT * from vect where transaction = $1 and n = $2 and spent = false FOR UPDATE;`,
                [
                    txid, vout
                ])
                if(unspentVout.rowCount == 0) return resolve(false);
                return resolve(unspentVout.rows[0]);
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        })
    }

    public SpendVout = (voutid: string, transaction: string, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                await client.query(`UPDATE public.vect
                SET spent=$3,consumed_tx=$2
                WHERE id=$1;`,
                [
                    voutid,
                    transaction,
                    true
                ]);
                return resolve(true);
            } catch (e) {
                console.log(e, 'SpendVout');
                return resolve(false);
            } 
        });
    }

    private uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    }
}
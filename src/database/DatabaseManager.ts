import { Pool, PoolClient } from 'pg';
const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASS,
    port: parseInt(process.env.DB_PORT),
});

import blockDatabase from './Block_Database';
const Block_Database = new blockDatabase(pool);

import transactionDatabase from './Transaction_Database';
const Transaction_Database = new transactionDatabase(pool);

import vectDatabase from './Vect_Database';
const Vect_Database = new vectDatabase(pool);

import addressDatabase from './Address_Database';
const Address_Database = new addressDatabase(pool);

export default class DatabaseManager {
    
    /**
     * Returns the Block Database
     * @returns {blockDatabase} The current Block database instance
     */
    public getBlockDatabase = ():blockDatabase => {
        return Block_Database;
    }

    public getTransactionDatabase = ():transactionDatabase => {
        return Transaction_Database;
    }

    public getVectDatabase = ():vectDatabase => {
        return Vect_Database;
    }

    public getAddressDatabase = ():addressDatabase => {
        return Address_Database;
    }
 
    public createClient = async ():Promise<PoolClient> => await pool.connect()

    public beginTransaction = async (client: PoolClient) => await client.query('BEGIN');

    public commitTransaction = async (client: PoolClient) => await client.query('COMMIT');

    public rollbackransaction = async (client: PoolClient) => await client.query('ROLLBACK');

    public uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    }
}
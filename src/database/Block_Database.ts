import {Pool, PoolClient} from 'pg';
import Block from '../models/Block';

export default class Block_Database {
    private pool : Pool;

    constructor(pool : Pool) {
        this.pool = pool;
    }

    public createBlock = (block: Block, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                await client.query(`INSERT INTO public.block(
                    hash, height, size, weight, versionhex, merkleroot, "time", nonce, bits, difficulty, chainwork, txcount, added)
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, now());`,
                [
                    block.getHash(),
                    block.getHeight(),
                    block.getSize(),
                    block.getWeight(),
                    block.getVersionHex(),
                    block.getMerkleRoot(),
                    block.getTime(),
                    block.getNonce(),
                    block.getBits(),
                    block.getDifficulty(),
                    block.getChainwork(),
                    block.getTransactions().length
                ]);
                return resolve(true);
            } catch (e) {
                console.log(e);
                return resolve(false);
            } 
        });
    }

    public getNextBlock = async () => {
        let value = await this.pool.query('select coalesce(max(height), 0) + 1 as current from block');
        return value.rows[0].current;
    }

    public getBlockCount = async () => {
        let { rows } = await this.pool.query(`SELECT count(1) as current FROM block`);
        return rows[0].current;
    }
}
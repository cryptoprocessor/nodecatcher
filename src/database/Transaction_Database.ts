import {Pool, PoolClient} from 'pg';
import Transaction from '../models/Transaction';
import Vect from '../models/Vect';

export default class Transaction_Database {
    private pool : Pool;

    constructor(pool : Pool) {
        this.pool = pool;
    }

    public createTransaction = (tx: Transaction, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                let txR = await client.query(`INSERT INTO public.transaction(
                    txid, hash, height, version, size, "time", vins, vouts, iswitness)
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);`,
                [
                    tx.getID(),
                    tx.getHash(),
                    tx.getHeight(),
                    tx.getVersion(),
                    tx.getSize(),
                    tx.getTime(),
                    [],
                    [],
                    tx.isWitness()
                ]);
                return resolve(true);
            } catch (e) {
                console.log(e, 'Create Transaction');
                return resolve(false);
            } 
        });
    }


    public updateVouts = (txid: string, vouts: String[], vins: String[], client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                await client.query(`UPDATE public.transaction
                SET vouts=$2, vins=$3
                WHERE txid=$1;`,
                [
                    txid,
                    vouts,
                    vins
                ])
                return resolve(true);
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        })
    }

    public updateFee = (txid: string, fee: number, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                await client.query(`UPDATE public.transaction
                SET fee=$2
                WHERE txid=$1;`,
                [
                    txid,
                    fee
                ])
                return resolve(true);
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        })
    }
}
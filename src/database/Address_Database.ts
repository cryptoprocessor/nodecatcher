import {Pool, PoolClient} from 'pg';
import Transaction from '../models/Transaction';
import Vect from '../models/Vect';

export default class Address_Database {
    private pool : Pool;

    constructor(pool : Pool) {
        this.pool = pool;
    }

    public createAddress = (address:string, received: number, sent: number, balance: number, height: number, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                let newAddress = await client.query(`INSERT INTO public.address(
                    address, received, sent, balance, first_block, created)
                    VALUES ($1, $2, $3, $4, $5, now()) RETURNING *;`,
                [
                    address,
                    received,
                    sent,
                    balance,
                    height
                ]);
                return resolve(true);
            } catch (e) {
                console.log(e);
                return resolve(false);
            } 
        });
    }
    
    public updateValues = (address:string, received: number, sent: number, balance: number, client: PoolClient) => {
        return new Promise(async (resolve) => {
            try {
                await client.query(`UPDATE public.address
                SET received=$2, sent=$3, balance=$4
                WHERE address = $1;`,
                [
                    address,
                    received,
                    sent,
                    balance
                ]);
                return resolve(true);
            } catch (e) {
                console.log(e);
                return resolve(false);
            } 
        });
    }

    public getAddress = (address: string, client: PoolClient):Promise<any> => {
        return new Promise(async (resolve) => {
            try {
                let addressRecord = await client.query(`SELECT * from address where address=$1 limit 1 for update;`,
                [
                    address
                ])
                if(addressRecord.rowCount == 0) return resolve(false);
                return resolve(addressRecord.rows[0]);
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        })
    }

    public getBalanceReconciliation = (client: PoolClient):Promise<number|false> => {
        return new Promise(async (resolve) => {
            try {
                let balance = await client.query(`select coalesce(sum(balance),0) from address`)
                return resolve(balance.rows[0]);
            } catch (e) {
                console.log(e);
                return resolve(false);
            }
        })
    }
}
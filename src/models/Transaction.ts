import Vect from "./Vect";
export default class Transaction {
    
    private txid: string;
    private hash: string;
    private height: number;
    private version: number;
    private size: number;
    private time: number;
    private Vins: any[]|null; //Null represents new coins. (Coinbase)
    private Vouts: Vect[];

    constructor(txid: string, hash: string, height: number, version: number, size: number, time: number, vins: Vect[]|null, vouts: Vect[]) {
        this.txid = txid;
        this.hash = hash;
        this.height = height;
        this.version = version;
        this.size = size;
        this.time = time;
        this.Vins = vins;
        this.Vouts = vouts;
    }

    public getID = () => this.txid;

    public getHash = () => this.hash;

    public getHeight = () => this.height;

    public getVersion = () => this.version;

    public getSize = () => this.size;

    public getTime = () => {
        let d = new Date(0);
        d.setUTCSeconds(this.time);
        return d;
    }

    public getVins = () => this.Vins;

    public getVouts = () => this.Vouts;

    public isWitness = () => this.txid !== this.hash;
}
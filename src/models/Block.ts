import Transaction from "./Transaction";

export default class Block {


    private hash: string;
    private height: number;
    private size: number;
    private weight: number;
    private versionHex: string;
    private merkleRoot: string;
    private transactions: Transaction[];
    private time: number;
    private nonce: number;
    private bits: string;
    private difficulty: number;
    private chainwork: string;

    constructor(hash: string, height: number, size: number, weight: number, versionHex: string, merkleRoot: string, time: number, nonce: number, bits: string, difficulty: number, chainwork: string) {
        this.hash = hash;
        this.height = height;
        this.size = size;
        this.weight = weight;
        this.versionHex = versionHex;
        this.merkleRoot = merkleRoot;
        this.time = time;
        this.nonce = nonce;
        this.bits = bits;
        this.difficulty = difficulty;
        this.chainwork = chainwork;
    }

    public getHash = () => this.hash;

    public getHeight = () => this.height;

    public getSize = () => this.size;

    public getWeight = () => this.weight;

    public getVersionHex = () => this.versionHex;

    public getMerkleRoot = () => this.merkleRoot;

    public getTransactions = () => this.transactions;

    public setTransactions = (transactions: Transaction[]) => this.transactions = transactions;

    public getTime = () => {
        let d = new Date(0);
        d.setUTCSeconds(this.time);
        return d;
    }

    public getNonce = () => this.nonce;

    public getBits = () => this.bits;

    public getDifficulty = () => this.difficulty;

    public getChainwork = () => this.chainwork;
}

/**
 * A 'Vect' represents an unspent / spent transaction output.
 * They are used in Transaction 'Vins' (Previous Transaction Outputs),
 * and are produced in the 'Vouts' of Transactions to be later spent.
 * 
 * They are referred to a 'Vect' as they are actually stored in a Vector, and it's just a short name.
 * *Couldn't actually find a technical term.
 * */
export default class Vect {

    private transaction: string;
    private value: number;
    private n: number;
    private type: string;
    private asm: string;
    private address: string = '';

    constructor (transasction: string, value: number, n: number, type: string, address: string, asm: string) {
        this.transaction = transasction;
        this.value = value;
        this.n = n;
        this.type = type;
        this.address = address;
        this.asm = asm;
    }


    public getTransaction = () => this.transaction;

    public getValue = () => this.value;

    public getN = () => this.n;

    public getType = () => this.type;

    public getAsm = () => this.asm;

    public getAddress = () => this.address;

    public isValidAddress = () => {
        //This could be improved with some form of Address Validation.
        return this.address !== '';
    }
}
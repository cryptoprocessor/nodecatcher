import {config as configDotenv} from 'dotenv';
configDotenv();
import * as SyncManager from "./sync/SyncManager";

const start = async () => {
    SyncManager.Start();
    SyncManager.StartMetricsLoop();
}

start();

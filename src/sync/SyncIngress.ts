import databaseManager from "../database/DatabaseManager";
const Database = new databaseManager();

import Block from "../models/Block";
import Transaction from "../models/Transaction";
import Vect from "../models/Vect";
import { PoolClient } from "pg";
import _ from "lodash";
let PreviousBlockHeight = 0;

export const DigestBlock = async (block: Block) => {
    const client = await Database.createClient();
    try {
        await Database.beginTransaction(client);
        //Just a quick check to confirm this block is next in the sequence
        //let blockSeqCount = await Database.getBlockDatabase().getBlockCount();
        let seqCountDiff = block.getHeight() - PreviousBlockHeight;
        if(seqCountDiff !== 1 && PreviousBlockHeight !== 0) throw new Error(`Digest Transaction Error (Sequence Out by ${seqCountDiff - 1})`);
        PreviousBlockHeight = block.getHeight();
        let createdBlock = await Database.getBlockDatabase().createBlock(block, client);
        if(!createdBlock) return false;

        //Digest Transactions
        //console.time('Transactions');
        for(let i = 0; i < block.getTransactions().length; i++) {
            let Digested = await DigestTransaction(block.getTransactions()[i], client);
            if(!Digested[0]) throw new Error(`Digest Transaction Error @ ${block.getTransactions()[i].getID()} ${block.getHeight()} = ${Digested[1]}`);
        }

        //Balance Reconcilitation
        //let balanceRecon = await Database.getAddressDatabase().getBalanceReconciliation(client) as any;
        //if(parseFloat(balanceRecon.coalesce) !== 0) throw new Error(`Digest Transaction Error ${block.getHeight()}`);
        //console.timeEnd('Transactions');
        //await Database.rollbackransaction(client);
        await Database.commitTransaction(client);
        return true;
    } catch (e) {
        console.log(e);
        await Database.rollbackransaction(client);
        return false;
    } finally {
        client.release();
    }
}


const DigestTransaction = async (Transaction: Transaction, client: PoolClient):Promise<[boolean, string]> => {
    return new Promise(async (resolve) => {
        //Create a transaction
        let createTransaction = await Database.getTransactionDatabase().createTransaction(Transaction, client);
        if(!createTransaction) return resolve([false, 'CREATE_TRANSACTION']);
        let SpentAmount = 0;
        const ProcessInputs = async () => {
            //Process Incoming (Vins)
            let vinAr:string[] = [];
            //For address creation and amount adjustments, if the vin is null it is refered to be a coinbase transaction
            //IE - Transaction for mined block.
            if(Transaction.getVins() == null) {
                //Coinbase transaction, get sum total of vouts to determine amount mined?
                let totalAmounts = await Transaction.getVouts().map((x:Vect) => x.getValue());
                let totalSum = totalAmounts.reduce((a, b) => a + b, 0);
                SpentAmount = totalSum;
                let PaymentProcess = await DigestTransfer('COINBASE', -totalSum, Transaction.getHeight(), client);
                if(!PaymentProcess) return false; //Error Occured
                return [];
            } else {
                //Iterate through inputs to be spent.
                //These are stored as objects. [{txid: '...', vout: 123}]
                for(let i = 0; i < Transaction.getVins().length; i++) {
                    let unspentVout = await Database.getVectDatabase().getUnspentVout(Transaction.getVins()[i].txid, Transaction.getVins()[i].vout, client) as any;
                    if(!unspentVout) return false; //Something is wrong?
                    //Make Deduction
                    SpentAmount += parseFloat(unspentVout.value);
                    let PaymentProcess = await DigestTransfer(unspentVout.address, -parseFloat(unspentVout.value), Transaction.getHeight(), client);
                    if(!PaymentProcess) return false; //Error Occured
                    //Mark Vout as Spent
                    let spendingVout = await Database.getVectDatabase().SpendVout(unspentVout.id, Transaction.getID(), client);
                    if(!spendingVout) return false;
                    //Push Vout to Array to be updated
                    vinAr.push(unspentVout.id);
                    //Claim previous as spent and add deduction.
                    if(i == Transaction.getVins().length - 1) {
                        return vinAr;
                    }
                }
            }
        }
        
        //Create new Outputs
        let ReceievedAmount = 0;
        const CreateOutputs = async () => {
            let voutAr: string[] = [];
            for(let i = 0; i < Transaction.getVouts().length; i++){
                let createdVout = await Database.getVectDatabase().createVect(Transaction.getVouts()[i], client);
                if(createdVout == false) return false;
                if(Transaction.getVouts()[i].isValidAddress()) {
                    //Action Payments
                    ReceievedAmount += Transaction.getVouts()[i].getValue();
                    let PaymentProcess = await DigestTransfer(Transaction.getVouts()[i].getAddress(), Transaction.getVouts()[i].getValue(), Transaction.getHeight(), client);
                    if(!PaymentProcess) return false; //Error Occured
                } else {
                    //Is an OP RETURN BURN
                }
                voutAr.push(createdVout);
                if(i == Transaction.getVouts().length - 1) {
                    return voutAr;
                }
            }
        }

        let Vins = await ProcessInputs();
        if(Vins === false) return resolve([false, 'PROCESS_VINS']);
        let Vouts = await CreateOutputs();
        if(Vouts == false) return resolve([false, 'CREATE_OUTPUTS']);

        //Update Transaction with Vouts
        let updateVouts = await Database.getTransactionDatabase().updateVouts(Transaction.getID(), Vouts, Vins as String[], client);
        if(updateVouts == false) return resolve([false, 'UPDATE_VOUTS']);

        //Fee amount is already sent to the Miner so send FEE to 'FEE' Address
        const Fee = parseFloat((SpentAmount - ReceievedAmount).toFixed(8));
        if(Fee !== 0) {
            let FeeProcess = await DigestTransfer('FEE', Fee, Transaction.getHeight(), client);
            if(FeeProcess == false) return resolve([false, 'ADD_FEES']);
            let txFeeUpdate = await Database.getTransactionDatabase().updateFee(Transaction.getID(), Fee, client);
            if(!txFeeUpdate) return resolve([false, 'UPDATE_FEES']);
        }
        return resolve([true, '']);
    });
}


const DigestTransfer = async (address: string, amount: number, height: number, client: PoolClient) => {
    //Get Address Record (If it doesn't exist it will return false and we'll create the record)
    let existingRecord = await Database.getAddressDatabase().getAddress(address, client);
    let Receieved = !existingRecord ? 0 : parseFloat(existingRecord.received);
    let Sent = !existingRecord ? 0 : parseFloat(existingRecord.sent);
    //Data Manipulation
    if(amount >= 0) {
        Receieved += amount;
        Receieved = parseFloat(Receieved.toFixed(8))
    } else {
        Sent += Math.abs(amount);
        Sent = parseFloat(Sent.toFixed(8))
    }
    let Balance = Receieved - Sent;
    if(Balance < 0 && address !== 'COINBASE') return false; //Would be a clear logical error.
    if(isNaN(Receieved) || isNaN(Sent) || isNaN(Balance)) return false;
    
    if(!existingRecord) {
        //New address, create address with correct amounts.
        let newAddress = await Database.getAddressDatabase().createAddress(address, Receieved, Sent, Balance, height, client);
        if(!newAddress) return false;
    } else {
        //Make calculations and update values.
        let updatedValues = await Database.getAddressDatabase().updateValues(address, Receieved, Sent, Balance, client);
        if(!updatedValues) return false;
    }
    return true;
}
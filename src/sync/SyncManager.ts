import databaseManager from "../database/DatabaseManager";
const Database = new databaseManager();

import * as SyncIngress from "./SyncIngress";
import RpcClient from "../rpc/RpcCLient";
import Block from "../models/Block";
import Transaction from "../models/Transaction";
import Vect from "../models/Vect";
const rClient = new RpcClient('127.0.0.1', 9332, 'yourusername', 'whateveryouwant');


let State: "Syncing" | "Stuck" | "Online" | "Starting" = "Starting";
let NodeHeight: number = 0;
let CurrentHeight: number = 0;
let Peers:String[] = [];
let NetHashPs = 0;

export const Start = async () => {
    //Starting Height is from database height.
    let startingHeight = await Database.getBlockDatabase().getNextBlock();
    //Start loop
    await BlockLoop(startingHeight);
}

/**
 * Collects Generation Metrics from the node
 * Such as Current Height, Peer Information etc.
 */
export const StartMetricsLoop = async () => {
    setInterval(async () => {
        let BlockchainInfo = await rClient.sendRPCRequest('getblockchaininfo');
        if(!BlockchainInfo.result.block && State == "Online") BlockLoop(BlockchainInfo.result.blocks);
        let Networkhashps = await rClient.sendRPCRequest('getnetworkhashps');
        NetHashPs = Networkhashps.result;
        let Balance = await rClient.sendRPCRequest('getbalance');
        let Peerinfo = await rClient.sendRPCRequest('getpeerinfo');
        Peers = Peerinfo.result.map((i:any) => i.addr);
        //console.log(Peers);
    }, 10000);
}

const BlockLoop = async (height: number) => {
    if(State !== "Syncing" && CurrentHeight >= height) return false;
    State = "Syncing";

    //Get hash from block id.
    let hashLookup = await rClient.sendRPCRequest('getblockhash', height);
    if(!hashLookup) return SyncError(height);
    if(hashLookup == 'Stable') {
        CurrentHeight = height - 1;
        //Double check the count of blocks, matches the last block.
        let blockSeqCount = await Database.getBlockDatabase().getBlockCount();
        let seqCountDiff = CurrentHeight - parseInt(blockSeqCount);
        if(seqCountDiff !== 0) {
            console.log('CRITICAL ERROR - Database Block Sequence out by', seqCountDiff, 'RPC Height:', CurrentHeight, 'DB Count:', blockSeqCount);
            throw new Error('CRITICAL ERROR - Database Block Sequence');
            return true;
        }
        console.log(seqCountDiff, CurrentHeight, blockSeqCount);

        State = 'Online'; //At latest, listening to latest.
        console.log(`Completed Syncing! Now at height ${CurrentHeight}`);
        return false;
    }
    //console.log('Processing Block', height);
    CurrentHeight = height;
    let hash = hashLookup.result;
    
    //RPC - Get Block Information
    let bInformation = await rClient.sendRPCRequest('getblock', hash);
    if(!bInformation) return SyncError(height);
    bInformation = bInformation.result;
    //Cast information to block.
    let BlockInstance = new Block(
        bInformation.hash,
        bInformation.height,
        bInformation.size,
        bInformation.weight,
        bInformation.versionHex,
        bInformation.merkleroot,
        bInformation.time,
        bInformation.nonce,
        bInformation.bits,
        bInformation.difficulty,
        bInformation.chainwork);
    
    //Loop through all of the transactions, and parse to Transaction Class.
    const getTransactions = async () => {
        let txs: Transaction[] = [];
        for(let i = 0; i < bInformation.tx.length; i++) {
            let tx = await getTransaction(bInformation.tx[i], bInformation.height);
            txs.push(tx);
            if(i == bInformation.tx.length - 1) {
                return txs;
            }
        }
    }
    let transactions = await getTransactions();
    if(!transactions) return SyncError(height);
    BlockInstance.setTransactions(transactions);
    //All Information has been Retrieved. 
    //Send to be Digested
    let DigestedBlock = await SyncIngress.DigestBlock(BlockInstance);
    if(!DigestedBlock) return SyncError(height);
    BlockLoop(height + 1);
}

const getTransaction = async (transaction: string, height: number):Promise<Transaction> => {
    return new Promise(async (resolve) => {
        let rawTransaction = await rClient.sendRPCRequest('getrawtransaction', transaction, -1);
        rawTransaction = rawTransaction.result;

        const getRecievingVects = async ():Promise<Vect[]> => {
            return rawTransaction.vout.map((i:any) => {
                let address = i.scriptPubKey.type !== 'nulldata' ? i.scriptPubKey.addresses[0] : '';
                return new Vect(rawTransaction.txid, i.value, i.n, i.scriptPubKey.type, address, i.scriptPubKey.asm);
            });
        }

        /**
         * A response of 'null' indicates a coinbase (Newly generated coins).
         */
        const getSendingVects = async ():Promise<Vect[]|null> => {
            //It is assumed a transaction with a coinbase, will only have generated coins within it.
            if(rawTransaction.vin[0].hasOwnProperty('coinbase')) return null;
            //Not a coinbase transaction, do a database lookup for previously unspent Vect.
            return rawTransaction.vin.map((i:any) => {
                return {txid: i.txid, vout: i.vout};
            });
        }

        let Vouts = await getRecievingVects();
        let Vins = await getSendingVects();
        let Trans = new Transaction(rawTransaction.txid, rawTransaction.hash, height, rawTransaction.version, rawTransaction.size, rawTransaction.time, Vins, Vouts);
        return resolve(Trans);
    });
}

const SyncError = async (height: number) => {
    console.log('Error during Sync - Height: ' + height);
}